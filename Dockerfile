FROM debian:10

ENV DOWNLOAD_URL https://github.com/SoftEtherVPN/SoftEtherVPN_Stable/releases/download/v4.34-9745-beta/softether-vpnserver-v4.34-9745-beta-2020.04.05-linux-x64-64bit.tar.gz

RUN apt-get update && apt-get install -y curl tar gzip grep make gcc cpp perl6-readline && \
    curl -L -o /opt/softether.tar.gz $DOWNLOAD_URL && \
    tar xzfp /opt/softether.tar.gz -C /opt && \
    cd /opt/vpnserver && \
    sed -i "s@cat@#cat@g" .install.sh && \
    make

COPY files/* /opt/
RUN chmod 755 /opt/*.sh

#ENTRYPOINT /bin/bash
ENTRYPOINT /opt/start.sh
